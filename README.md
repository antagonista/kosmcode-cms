# Docker

* enter to bash container (db) : `docker-compose exec db bash`
* enter to bash container (app) : `docker-compose exec app <command>`

* migrate : `docker-compose exec app php artisan migrate`

# Usages
* Site: https://github.com/BlackrockDigital/startbootstrap-bare
* Admin Dashboard: https://github.com/vikdiesel/admin-one-vue-bulma-dashboard


