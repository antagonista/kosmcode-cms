<?php

namespace App\Http\Controllers;

use http\Env\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest as LoginRequest;

class UserController extends Controller
{
    /**
     * Login Form
     *
     * @return JsonResponse
     */
    public function loginForm()
    {

        $form = new LoginRequest();

        return response()->json($form->generateFormResponse(), 200);
    }
}
