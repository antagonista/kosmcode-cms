<?php

namespace App\Http\Requests;

class LoginRequest extends VueFormRequest
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Form Login Schema
     *
     * @return array
     */
    public function getFormSchema()
    {
        return [
            'fields' => [
                [
                    'model'=> 'email',
                    'type' => 'input',
                    'inputType' => 'email',
                    'label' => 'E-mail',
                    'placeholder' => 'User\'s e-mail address',
                    'required' => true,
                    'validator' => ['required', 'email']
                ],
                [
                    'model'=> 'password',
                    'type' => 'input',
                    'inputType' => 'password',
                    'label' => 'Password',
                    'placeholder' => 'User\'s password',
                    'required' => true,
                    'min' => 8,
                    'hint' => 'Minimum 6 characters',
                    'validator' => ['required', 'string']
                ]
            ]
        ];
    }

    /**
     * Form Login Model
     *
     * @return array
     */
    public function getFormModel()
    {
        return [
            'email' => null,
            'password' => null
        ];
    }
}
