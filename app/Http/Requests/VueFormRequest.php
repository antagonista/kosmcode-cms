<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VueFormRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get Form Schema for Vue Form Generator
     *
     * @return array
     */
    public function getFormSchema()
    {
        return [];
    }

    /**
     * Get Form Model for Vue Form Generator
     *
     * @return array
     */
    public function getFormModel()
    {
        return [];
    }

    /**
     * Get Form Options for Vue Form Generator
     *
     * @return array
     */
    public function getFormOptions()
    {
        return [
            'validateAfterLoad' => false,
            'validateAfterChanged' => true
        ];
    }

    /**
     * Get Form Response for Vue Form Generator
     *
     * @return array
     */
    public function generateFormResponse()
    {
        return [
            'schema' => $this->getFormSchema(),
            'model' => $this->getFormModel(),
            'options' => $this->getFormOptions()
        ];
    }
}
