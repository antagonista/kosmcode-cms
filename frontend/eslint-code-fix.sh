#!/bin/sh

find src/ -regex ".*\.\(vue\|js\)"

# shellcheck disable=SC2044
for FILE in $(find src/ -regex ".*\.\(vue\|js\)")
do
    #./node_modules/.bin/eslint --fix "./$FILE"
    ./node_modules/.bin/eslint --fix "./$FILE"
done
