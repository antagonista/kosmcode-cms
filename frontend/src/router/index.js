import Vue from 'vue'
import VueRouter from 'vue-router'
import IndexAdmin from '../IndexAdmin'
import IndexSite from '../IndexSite'

Vue.use(VueRouter)

const routes = [
  {
    path: '',
    component: IndexSite,
    children: [
      {
        path: '/',
        name: 'Home',
        component: () => import(/* webpackChunkName: "Home" */ '../views/Home.vue')
      },
      {
        path: 'login',
        name: 'Login',
        component: () => import(/* webpackChunkName: "Login" */ '../views/Login.vue')
      },
      {
        path: 'register',
        name: 'Register',
        component: () => import(/* webpackChunkName: "Register" */ '../views/Register.vue')
      }
    ]
  },
  {
    path: '/admin',
    component: IndexAdmin,
    children: [
      {
        // Document title tag
        // We combine it with defaultDocumentTitle set in `src/main.js` on router.afterEach hook
        meta: {
          title: 'Admin Dashboard'
        },
        path: '/',
        name: 'AdminHome',
        component: () => import(/* webpackChunkName: "adminHome" */ '../views/admin/Home.vue')
      },
      {
        meta: {
          title: 'Tables'
        },
        path: 'tables',
        name: 'tables',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "tables" */ '../views/admin/Tables.vue')
      },
      {
        meta: {
          title: 'Forms'
        },
        path: 'forms',
        name: 'forms',
        component: () => import(/* webpackChunkName: "forms" */ '../views/admin/Forms.vue')
      },
      {
        meta: {
          title: 'Profile'
        },
        path: 'profile',
        name: 'profile',
        component: () => import(/* webpackChunkName: "profile" */ '../views/admin/Profile.vue')
      },
      {
        meta: {
          title: 'New Client'
        },
        path: 'client/new',
        name: 'client.new',
        component: () => import(/* webpackChunkName: "client-form" */ '../views/admin/ClientForm.vue')
      },
      {
        meta: {
          title: 'Edit Client'
        },
        path: 'client/:id',
        name: 'client.edit',
        component: () => import(/* webpackChunkName: "client-form" */ '../views/admin/ClientForm.vue'),
        props: true
      }
    ]
  }

]

const router = new VueRouter({
  base: process.env.BASE_URL,
  routes,
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
})

export default router
