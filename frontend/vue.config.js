module.exports = {
  publicPath: process.env.DEPLOY_ENV === 'GH_PAGES'
    ? '/admin-one-vue-bulma-dashboard/'
    : '/',

  outputDir: '../public',
  // modify the location of the generated HTML file.
  // make sure to do this only in production.
  indexPath:
        process.env.NODE_ENV === 'production'
          ? '../resources/views/app.blade.php'
          : 'index.html'
}
