#!/bin/sh

RULESET=phpcs.xml

echo "\nValidating PHPCS:\n"
./vendor/bin/phpcs --standard="$RULESET"

echo "\nFIXED PHPCBF:\n"
./vendor/bin/phpcbf --standard="$RULESET"

echo "\nPHPCS validation completed!\n"
